## Sample Blog
##### Powered by Adv Softlogic


This is a simple example blog.

You can clone the repository yourself.

##### Language used : *Angular*

##### Framework used : *Ionic*

#### Steps:
- **clone** ***the repostory***
- **install the *Ionic* framework**
- **run: *npm install***


#### Start the local server to display the blog on the web

**run: ionic serve**
