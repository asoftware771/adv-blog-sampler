import { Component } from '@angular/core';
import { NavController, NavParams } from '@ionic/angular';
import { NavigationExtras, Router } from '@angular/router';
import { IonicModule } from '@ionic/angular';
import { Observable } from 'rxjs';

@Component({
  selector: 'app-home',
  templateUrl: 'home.page.html',
  styleUrls: ['home.page.scss'],
})
export class HomePage {

  articoles = [
    {
      title: "Article1",
      subtitle: "This is Subtitle",
      lecture: "Lorem ipsum dolor sit",
      preview: "Lorem ipsum dolor sit"
    },
    {
      title: "Lorem ipsum",
      subtitle: "Lorem ipsum dolor sit",
      lecture: "Lorem ipsum dolor sit",
      preview: "Lorem ipsum dolor sit"
    },
    {
      title: "Lorem ipsum",
      subtitle: "Lorem ipsum dolor sit amet",
      lecture: "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Cum sociis natoque penatibus et magnis dis parturient. Nisl tincidunt eget nullam non. Turpis massa tincidunt dui ut ornare. Dignissim diam quis enim lobortis scelerisque fermentum dui faucibus in. Nisi porta lorem mollis aliquam ut porttitor leo. In hac habitasse platea dictumst vestibulum rhoncus est pellentesque elit. Quis risus sed vulputate odio ut enim blandit. Nunc sed blandit libero volutpat sed. Neque aliquam vestibulum morbi blandit cursus risus at ultrices. Enim lobortis scelerisque fermentum dui faucibus in. Lectus urna duis convallis convallis tellus. Quis ipsum suspendisse ultrices gravida dictum fusce ut. Quis hendrerit dolor magna eget.",
      preview: "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Cum sociis natoque penatibus et magnis dis parturient. Nisl tincidunt eget nullam non. Turpis massa tincidunt dui ut ornare. Dignissim diam quis enim lobortis scelerisque fermentum dui faucibus in. Nisi porta lorem mollis aliquam ut porttitor leo. In hac habitasse platea dictumst vestibulum rhoncus est pellentesque elit. Quis risus sed vulputate odio ut enim blandit. Nunc sed blandit libero volutpat sed. Neque aliquam vestibulum morbi blandit cursus risus at ultrices...",
      image1: "../../assets/img/pexels-luis-del-río-15286.jpg",
      image2: "../../assets/img/pexels-jacob-colvin-1761279.jpg"
    }
  ];

  
  constructor(private router:Router) {}


  openArticle(article) {
    const params: NavigationExtras = {
      queryParams: {
        title: article.title,
        subtitle: article.subtitle,
        lecture: article.lecture,
        image1: article.image1,
        image2: article.image2,
      }
    }
    this.router.navigate(['/article'], params);
  }

}
