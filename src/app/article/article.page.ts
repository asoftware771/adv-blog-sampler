import { Component, OnInit } from '@angular/core';
import { NavController, NavParams } from '@ionic/angular';
import { ActivatedRoute, Router } from '@angular/router';

@Component({
  selector: 'app-article',
  templateUrl: './article.page.html',
  styleUrls: ['./article.page.scss'],
})
export class ArticlePage implements OnInit {
  article: any;
  relatedArticle = [
    {
      title: "Lorem ipsum",
      subtitle: "Lorem ipsum dolor sit",
      lecture: 'Lorem ipsum dolor sit',
    },
    {
      title: "Lorem ipsum",
      subtitle: "Lorem ipsum dolor sit",
      lecture: 'Lorem ipsum dolor sit',
    },
    {
      title: "Lorem ipsum",
      subtitle: "Lorem ipsum dolor sit",
      lecture: 'Lorem ipsum dolor sit',
    },
    {
      title: "Lorem ipsum",
      subtitle: "Lorem ipsum dolor sit",
      lecture: 'Lorem ipsum dolor sit',
    },
    {
      title: "Lorem ipsum",
      subtitle: "Lorem ipsum dolor sit",
      lecture: 'Lorem ipsum dolor sit',
    },
    {
      title: "Lorem ipsum",
      subtitle: "Lorem ipsum dolor sit",
      lecture: 'Lorem ipsum dolor sit',
    },
  ];

  slideOpts = {
    initialSlide: 0,
    speed: 400
  };

  constructor(private router: Router, private route: ActivatedRoute) {
    this.article = this.router.getCurrentNavigation().extras.queryParams;
  }

  ngOnInit() {}
}
