import { NgModule } from "@angular/core";
import { ToolbarmenuComponent } from "./toolbarmenu/toolbarmenu.component";
import { FooterComponent } from "./footer/footer.component";

@NgModule({
    declarations:[ToolbarmenuComponent,FooterComponent],
    exports:[ToolbarmenuComponent,FooterComponent]
})

export class ComponentsModule{}