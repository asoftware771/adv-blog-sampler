import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-toolbarmenu',
  templateUrl: './toolbarmenu.component.html',
  styleUrls: ['./toolbarmenu.component.scss'],
})
export class ToolbarmenuComponent implements OnInit {
 
  constructor(private router:Router) { }

  ngOnInit() {}

  openNews() {
    this.router.navigate(['/articleview']);
  }

  openHome() {
    this.router.navigate(['/home']);
  }

}
