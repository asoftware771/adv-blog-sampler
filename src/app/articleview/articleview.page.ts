import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-articleview',
  templateUrl: './articleview.page.html',
  styleUrls: ['./articleview.page.scss'],
})
export class ArticleviewPage implements OnInit {

  articoles = [
    {
      title: "Lorem ipsum",
      subtitle: "Lorem ipsum dolor sit amet",
      lecture: "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Cum sociis natoque penatibus et magnis dis parturient. Nisl tincidunt eget nullam non. Turpis massa tincidunt dui ut ornare. Dignissim diam quis enim lobortis scelerisque fermentum dui faucibus in. Nisi porta lorem mollis aliquam ut porttitor leo. In hac habitasse platea dictumst vestibulum rhoncus est pellentesque elit. Quis risus sed vulputate odio ut enim blandit. Nunc sed blandit libero volutpat sed. Neque aliquam vestibulum morbi blandit cursus risus at ultrices. Enim lobortis scelerisque fermentum dui faucibus in. Lectus urna duis convallis convallis tellus. Quis ipsum suspendisse ultrices gravida dictum fusce ut. Quis hendrerit dolor magna eget.",
      preview: "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Cum sociis natoque penatibus et magnis dis parturient. Nisl tincidunt eget nullam non. Turpis massa tincidunt dui ut ornare. Dignissim diam quis enim lobortis scelerisque fermentum dui faucibus in. Nisi porta lorem mollis aliquam ut porttitor leo. In hac habitasse platea dictumst vestibulum rhoncus est pellentesque elit. Quis risus sed vulputate odio ut enim blandit. Nunc sed blandit libero volutpat sed. Neque aliquam vestibulum morbi blandit cursus risus at ultrices...",
      image1: "../../assets/img/pexels-luis-del-río-15286.jpg",
      image2: "../../assets/img/pexels-jacob-colvin-1761279.jpg"
    },{
      title: "Lorem ipsum dolor sit amet",
      subtitle: "Lorem ipsum dolor sit amet",
      lecture: "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Cum sociis natoque penatibus et magnis dis parturient. Nisl tincidunt eget nullam non. Turpis massa tincidunt dui ut ornare. Dignissim diam quis enim lobortis scelerisque fermentum dui faucibus in. Nisi porta lorem mollis aliquam ut porttitor leo. In hac habitasse platea dictumst vestibulum rhoncus est pellentesque elit. Quis risus sed vulputate odio ut enim blandit. Nunc sed blandit libero volutpat sed. Neque aliquam vestibulum morbi blandit cursus risus at ultrices. Enim lobortis scelerisque fermentum dui faucibus in. Lectus urna duis convallis convallis tellus. Quis ipsum suspendisse ultrices gravida dictum fusce ut. Quis hendrerit dolor magna eget.",
      preview: "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Cum sociis natoque penatibus et magnis dis parturient. Nisl tincidunt eget nullam non. Turpis massa tincidunt dui ut ornare. Dignissim diam quis enim lobortis scelerisque fermentum dui faucibus in. Nisi porta lorem mollis aliquam ut porttitor leo. In hac habitasse platea dictumst vestibulum rhoncus est pellentesque elit. Quis risus sed vulputate odio ut enim blandit. Nunc sed blandit libero volutpat sed. Neque aliquam vestibulum morbi blandit cursus risus at ultrices...",
      image1: "../../assets/img/pexels-arthouse-studio-4534200.jpg",
      image2: "../../assets/img/pexels-jacob-colvin-1761279.jpg"
    },
    {
      title: "Consectetur adipiscing elit, sed do eiusmod tempor",
      subtitle: "Lorem ipsum dolor sit amet",
      lecture: "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Cum sociis natoque penatibus et magnis dis parturient. Nisl tincidunt eget nullam non. Turpis massa tincidunt dui ut ornare. Dignissim diam quis enim lobortis scelerisque fermentum dui faucibus in. Nisi porta lorem mollis aliquam ut porttitor leo. In hac habitasse platea dictumst vestibulum rhoncus est pellentesque elit. Quis risus sed vulputate odio ut enim blandit. Nunc sed blandit libero volutpat sed. Neque aliquam vestibulum morbi blandit cursus risus at ultrices. Enim lobortis scelerisque fermentum dui faucibus in. Lectus urna duis convallis convallis tellus. Quis ipsum suspendisse ultrices gravida dictum fusce ut. Quis hendrerit dolor magna eget.",
      preview: "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Cum sociis natoque penatibus et magnis dis parturient. Nisl tincidunt eget nullam non. Turpis massa tincidunt dui ut ornare. Dignissim diam quis enim lobortis scelerisque fermentum dui faucibus in. Nisi porta lorem mollis aliquam ut porttitor leo. In hac habitasse platea dictumst vestibulum rhoncus est pellentesque elit. Quis risus sed vulputate odio ut enim blandit. Nunc sed blandit libero volutpat sed. Neque aliquam vestibulum morbi blandit cursus risus at ultrices...",
      image1: "../../assets/img/pexels-tobias-bjørkli-1693095.jpg",
      image2: "../../assets/img/pexels-jacob-colvin-1761279.jpg"
    },
    {
      title: "Lorem ipsum",
      subtitle: "Lorem ipsum dolor sit amet",
      lecture: "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Cum sociis natoque penatibus et magnis dis parturient. Nisl tincidunt eget nullam non. Turpis massa tincidunt dui ut ornare. Dignissim diam quis enim lobortis scelerisque fermentum dui faucibus in. Nisi porta lorem mollis aliquam ut porttitor leo. In hac habitasse platea dictumst vestibulum rhoncus est pellentesque elit. Quis risus sed vulputate odio ut enim blandit. Nunc sed blandit libero volutpat sed. Neque aliquam vestibulum morbi blandit cursus risus at ultrices. Enim lobortis scelerisque fermentum dui faucibus in. Lectus urna duis convallis convallis tellus. Quis ipsum suspendisse ultrices gravida dictum fusce ut. Quis hendrerit dolor magna eget.",
      preview: "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Cum sociis natoque penatibus et magnis dis parturient. Nisl tincidunt eget nullam non. Turpis massa tincidunt dui ut ornare. Dignissim diam quis enim lobortis scelerisque fermentum dui faucibus in. Nisi porta lorem mollis aliquam ut porttitor leo. In hac habitasse platea dictumst vestibulum rhoncus est pellentesque elit. Quis risus sed vulputate odio ut enim blandit. Nunc sed blandit libero volutpat sed. Neque aliquam vestibulum morbi blandit cursus risus at ultrices...",
      image1: "../../assets/img/pexels-s-migaj-1402850.jpg",
      image2: "../../assets/img/pexels-jacob-colvin-1761279.jpg"
    },
    {
      title: "Sed do eiusmod tempor incididunt",
      subtitle: "Lorem ipsum dolor sit amet",
      lecture: "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Cum sociis natoque penatibus et magnis dis parturient. Nisl tincidunt eget nullam non. Turpis massa tincidunt dui ut ornare. Dignissim diam quis enim lobortis scelerisque fermentum dui faucibus in. Nisi porta lorem mollis aliquam ut porttitor leo. In hac habitasse platea dictumst vestibulum rhoncus est pellentesque elit. Quis risus sed vulputate odio ut enim blandit. Nunc sed blandit libero volutpat sed. Neque aliquam vestibulum morbi blandit cursus risus at ultrices. Enim lobortis scelerisque fermentum dui faucibus in. Lectus urna duis convallis convallis tellus. Quis ipsum suspendisse ultrices gravida dictum fusce ut. Quis hendrerit dolor magna eget.",
      preview: "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Cum sociis natoque penatibus et magnis dis parturient. Nisl tincidunt eget nullam non. Turpis massa tincidunt dui ut ornare. Dignissim diam quis enim lobortis scelerisque fermentum dui faucibus in. Nisi porta lorem mollis aliquam ut porttitor leo. In hac habitasse platea dictumst vestibulum rhoncus est pellentesque elit. Quis risus sed vulputate odio ut enim blandit. Nunc sed blandit libero volutpat sed. Neque aliquam vestibulum morbi blandit cursus risus at ultrices...",
      image1: "../../assets/img/pexels-trace-hudson-2770933.jpg",
      image2: "../../assets/img/pexels-jacob-colvin-1761279.jpg"
    }
  ];
  
  slideOpts = {
    initialSlide: 0,
    speed: 400
  };
  
  constructor() { }

  ngOnInit() {
  }

}
