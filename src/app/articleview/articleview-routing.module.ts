import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { ArticleviewPage } from './articleview.page';

const routes: Routes = [
  {
    path: '',
    component: ArticleviewPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class ArticleviewPageRoutingModule {}
